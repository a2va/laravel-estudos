-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.11-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para crudteste
CREATE DATABASE IF NOT EXISTS `crudteste` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `crudteste`;

-- Copiando estrutura para tabela crudteste.contatos
CREATE TABLE IF NOT EXISTS `contatos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profissao` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_social` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ativo','inativo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ativo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela crudteste.contatos: ~25 rows (aproximadamente)
/*!40000 ALTER TABLE `contatos` DISABLE KEYS */;
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(2, 'Adriana A2va', '', '', '', 'ativo', '2020-01-21 23:20:38', '2020-01-22 00:04:12');
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(3, 'Adriana Teste', '', '', '', 'ativo', '2020-01-21 23:21:10', '2020-01-21 23:21:10');
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(4, 'Adriana Viana', '', '', '', 'ativo', '2020-01-22 00:10:40', '2020-01-22 00:10:40');
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(5, 'Adriana Viana', '', '', '', 'ativo', '2020-01-22 00:10:40', '2020-01-22 00:10:40');
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(6, 'Adriana Viana', '', '', '', 'ativo', '2020-01-22 00:10:40', '2020-01-22 00:10:40');
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(7, 'Adriana Viana', '', '', '', 'ativo', '2020-01-22 00:10:40', '2020-01-22 00:10:40');
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(8, 'Adriana Viana', '', '', '', 'ativo', '2020-01-22 00:10:40', '2020-01-22 00:10:40');
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(9, 'Adriana Viana', '', '', '', 'ativo', '2020-01-22 00:10:40', '2020-01-22 00:10:40');
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(10, 'Adriana Viana', '', '', '', 'ativo', '2020-01-22 00:10:40', '2020-01-22 00:10:40');
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(11, 'Adriana Viana', 'F', 'Desenvolvedora de sistemas', 'Não possui.,', 'ativo', '2020-01-22 00:10:40', '2020-01-28 21:51:18');
INSERT IGNORE INTO `contatos` (`id`, `nome`, `sexo`, `profissao`, `link_social`, `status`, `created_at`, `updated_at`) VALUES
	(28, 'Lucas Luiz', 'M', 'Pedreiro', 'https://getbootstrap.com/', 'ativo', '2020-01-28 21:03:52', '2020-01-28 21:31:12');
/*!40000 ALTER TABLE `contatos` ENABLE KEYS */;

-- Copiando estrutura para tabela crudteste.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela crudteste.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Copiando estrutura para tabela crudteste.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela crudteste.migrations: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1);
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(4, '2020_01_21_145219_create_agendas_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Copiando estrutura para tabela crudteste.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela crudteste.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Copiando estrutura para tabela crudteste.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela crudteste.users: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Adriana Dev', 'al.adriana.v@gmail.com', NULL, '$2y$10$t5.BN1aIZOljksW79GI9t.Od21qnD7fduiKOuF5zrR1XfjLd3xiEK', NULL, '2020-01-21 16:11:24', '2020-01-21 16:11:24');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
