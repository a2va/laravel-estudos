<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agenda;

class AgendaController extends Controller
{

    private $agenda;
    
    public function __construct(Agenda $agenda){
        
        $this->agenda = $agenda;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contatos = $this->agenda->orderBy('id', 'DESC')->paginate(5);
        return view('agenda.index', compact('contatos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agenda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $data = $request->all();
            //Criado com Mass Assignment
            $agenda = $this->agenda->create($data);

            flash('Contato salvo com sucesso!')->success();
            return redirect()->route('agenda.index');

        } catch(\Exception $e) {
            if(env('APP_DEBUG')) {
                flash($e->getMessage())->warning();

                return redirect()->back();
            }

            flash('Contato não foi criada, tente novamente ou chame o suporte!')->warning();
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $agenda = $this->agenda->findOrFail($id);

            return view('agenda.edit', compact('agenda'));

        } catch(\Exception $e) {
            if(env('APP_DEBUG')) {
                flash($e->getMessage())->warning();
                return redirect()->back();
            }

            flash('Post não encontrado...')->warning();
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $data = $request->all();

            $agenda = $this->agenda->findOrFail($id);
            $agenda->update($data);

            flash('Contato atualizado com sucesso!')->success();
            return redirect()->route('agenda.index');

        } catch(\Exception $e) {
            if(env('APP_DEBUG')) {
                flash($e->getMessage())->warning();
                return redirect()->back();
            }
            flash('Contato não foi atualizado...')->warning();
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $agenda = $this->agenda->findOrFail($id);
            $agenda->delete();

            flash('Contato removido com sucesso!')->success();
            return redirect()->route('agenda.index');

        } catch(\Exception $e) {
            if(env('APP_DEBUG')) {
                flash($e->getMessage())->warning();
                return redirect()->back();
            }

            flash('Contato não pode ser removido...')->warning();
            return redirect()->back();
        }
    }
}
