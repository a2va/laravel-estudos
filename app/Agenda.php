<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $fillable = [ 'nome','sexo','profissao','link_social','status' ];
    protected $table = 'contatos';
}
