@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="col-12">
            <a href="{{route('agenda.create')}}" class="btn btn-success">Adicionar Contato</a>
        </div>
        <br>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Sexo</th>
                    <th>Profissão</th>
                    <th>Link Social</th>
                    <th>Status</th>
                    <th>Criado</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @forelse($contatos as $contato)
                <tr>
                    <td>{{$contato->id}}</td>
                    <td>{{$contato->nome}}</td>
                    <td>{{$contato->sexo}}</td>
                    <td>{{$contato->profissao}}</td>
                    <td>{{$contato->link_social}}</td>
                    <td>{{$contato->status}}</td>
                    <td>{{$contato->created_at}}</td>
                    <td>
                        <a href="{{route('agenda.show', ['agenda' => $contato->id ])}}" class="btn btn-secondary">Editar</a>                        
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#excluirContato">Excluir</button>

                        <div class="modal" tabindex="-1" role="dialog" id="excluirContato">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Excluir Contato</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h4>Deseja realmente exlcuir contato?</h4>
                                </div>
                                <div class="modal-footer">                                    
                                    <form action="{{route('agenda.destroy', ['agenda' => $contato->id])}}" method="post">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Excluir</button>
                                    </form> 
                                </div>
                                </div>
                            </div>
                        </div>        

                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="7">
                        <h2>Nenhum contato encontrado!</h2>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
        {{ $contatos->links() }}
    </div>

@endsection