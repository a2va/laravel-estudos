@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="col-12">
            <h4>Editando Novo Contato</h4>
            <a href="{{route('agenda.index')}}" class="float-right btn btn-secondary">Retornar</a>
        </div>
        <br>
        <form action="{{route('agenda.update', ['agenda' => $agenda->id])}}" method="post">
        @csrf
        @method("PUT")
            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" name="nome" id="nome" value="{{$agenda->nome}}" class="form-control">
            </div>

            <div class="form-group">
                <label for="sexo">Sexo</label>                
                <select name="sexo" id="sexo" class="form-control">
                    <option value="F" {{$agenda->sexo == 'F' ? 'selected' : ''}}>Feminino</option>
                    <option value="M" {{$agenda->sexo == 'M' ? 'selected' : ''}}>Masculino</option>
                </select>
            </div>            

            <div class="form-group">
                <label for="profissao">Profissão</label>
                <input type="text" name="profissao" id="profissao" value="{{$agenda->profissao}}" class="form-control" placeholder="Analista de Sistema, Médico, Pedreiro, Padeiro ou etc.">
            </div>

            <div class="form-group">
                <label for="link_social">Link Social</label>
                <input type="text" name="link_social" id="link_social" value="{{$agenda->link_social}}" class="form-control" placeholder="Seu instagran, facebook, site pessoal, linkdin ou etc.">
            </div>

            <div class="form-group">
                <label for="status">Status</label>                
                <select name="status" id="status" class="form-control">
                    <option value="ativo" {{$agenda->status == 'ativo' ? 'selected' : ''}}>Ativo</option>
                    <option value="inativo" {{$agenda->status == 'inativo' ? 'selected' : ''}}>Inativo</option>
                </select>
            </div>

            <button type="submit" class="btn btn-success">Salvar Edição</button>
        </form>
    </div>

@endsection