<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Adriana V - CRUD</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <link rel="stylesheet" href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://getbootstrap.com/docs/4.0/examples/cover/cover.css">

    </head>

    <body class="text-center">

        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            <header class="masthead mb-auto">
                <div class="inner">
                <h3 class="masthead-brand">Adriana V - CRUD</h3>
                <nav class="nav nav-masthead justify-content-center">
                    <a class="nav-link active" href="{{ url('/login') }}">Login</a>
                    <a class="nav-link" href="{{ route('register') }}">Cadastrar-se</a>
                    <a class="nav-link" href="">Contato</a>
                </nav>
                </div>
            </header>

            <main role="main" class="inner cover">
                <h1 class="cover-heading">Olá, bem vindo(a)!</h1>
                <p class="lead">Colocando em prático o que foi aprendido, este projeto consiste em uma mini agenda de contatos. </p>
                <p class="lead">
                <a href="{{ url('/login') }}" class="btn btn-lg btn-secondary">Logar para Saber mais!</a>
                </p>
            </main>

            <footer class="mastfoot mt-auto">
                <div class="inner">
                <p>Cover template for <a href="https://getbootstrap.com/">Bootstrap</a>, by <a href="https://twitter.com/mdo">@mdo</a>.</p>
                </div>
            </footer>

        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        
        <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
        <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
    </body>         
            
</html>
